import numpy as np
from ART import norm, data_validity_check
import sys

class ellipse:
    def __init__(self,a=1,b=1,x=0,y=0,theta=0):
        self.a = abs(a)
        self.b = abs(b)
        self.x = x
        self.y = y
        self.theta = theta

    def points(self):
        if self.a > 0 and self.b > 0:
            x = np.arange(-self.a, self.a,self.a/100)
            y1 = self.b * np.sqrt((1 - ((x / self.a) ** 2)))
            y2 = -y1
            x = np.concatenate((x,x[::-1]))
            y = np.concatenate((y1,y2[::-1]))
            c, s = np.cos(self.theta), np.sin(self.theta)
            R = np.array(((c, -s), (s, c)))
            xy = np.stack((x, y)).transpose()
            xy = np.matmul(R, xy.transpose()).transpose()
            return (xy[:, 0]+self.x, xy[:, 1]+self.y)
        else:
            return [self.x,self.y]

class EllipsoidalART:
    def __init__(self,rho,a,D,mu,nu):
        self.rho = rho
        self.a = a
        self.D = D
        self.mu = mu
        self.nu = nu
        self.m = None
        self.d = None
        self.R = 0
        self.hist = np.zeros(0)
        self.bins = []
        self.n_c = 0

    def dist(self ,X ,c):
        XM = np.subtract(X,self.m[c,:])
        if np.count_nonzero(self.d[c,:]) == 0:
            return norm(XM,2)
        else:
            a = (1/self.mu)
            b = norm(XM,2)**2
            cc = (1 - self.mu**2)

            d = np.dot(self.d[c,:], XM**2)
            ans = a*np.sqrt(b - cc*d)

            return ans

    def max_dis(self,X,c):
        return max(self.dist(X,c),self.R[c])


    def weight_update(self ,X ,c):
        """
        Updates the weights of cluster c to reflect the addition of X. Ellipsoidal clustering only
        :param X: an m-dim feature vector
        :param c: the index of the cluster to update
        :return: None
        """
        m = X.size
        if self.m is None:
            self.m = np.reshape(np.copy(X),(1,-1))
            self.d = np.zeros((1,m))
            self.R = np.zeros((1))
            self.n_c = 1
        elif c >= self.n_c:
            self.m = np.vstack((self.m ,np.copy(X)))
            self.d = np.vstack((self.d ,np.zeros((1,m))))
            self.R = np.concatenate((self.R,np.zeros(1)))
            self.n_c = self.m.shape[0]
        else:
            if not np.all(np.equal(X,self.m[c,:])):
                if np.count_nonzero(self.d[c,:]) == 0:
                    self.d[c,:] = (X-self.m[c,:])/norm(X-self.m[c,:],2)
                a = (self.nu / 2)
                dist = self.dist(X,c)
                b = min(self.R[c], dist)
                d = max(self.R[c], dist)
                if dist > self.R[c]:
                    cc = (X-self.m[c,:])
                    self.m[c,:] += a*(1-(b/dist))*cc
                self.R[c] += a * (d - self.R[c])

    def F1c(self,X,c):
        """
        :param X: an m-dim feature vector
        :param c: the cluster index of interest
        :return: the F1 activation of X on cluster c
        """
        return (self.D-self.R[c]-self.max_dis(X,c))/(self.D-2*self.R[c]+self.a)

    def F2c(self,X,c):
        """
        :param X: an m-dim feature vector
        :param c: the cluster index of interest
        :return: the F2 activation of X on cluster c
        """
        return (1 - (self.R[c]+self.max_dis(X,c))/self.D) > self.rho

    def F1(self,X):
        """
        :param X: an m-dim feature vector
        :return: the F1 activation of X on each of the clusters
        """
        F1 = np.zeros(self.n_c)
        for c in range(self.n_c):
            F1[c] = self.F1c(X,c)
        return F1

    def find_winning_cluster(self, X, res2func=None):
        """
        :param X: an m-dim feature vector
        :param res2func: an optional secondary resonance function f(c). for supervised learning.
        :return: the index of the winning cluster
        """
        if self.n_c == 0:
            self.hist = np.ones(1)
            return 0
        F1 = self.F1(X)
        mi = np.argmax(F1)
        while (not self.F2c(X,mi)) or (res2func is not None and not res2func(mi)):
            F1[mi] = -np.inf
            if np.all(np.isinf(F1)):
                break
            mi = np.argmax(F1)

        if F1[mi] > 0:
            self.hist[mi] += 1
            return mi
        else:
            if self.hist.shape[0] <= self.n_c:
                self.hist = np.concatenate((self.hist,np.ones(1)))
            else:
                self.hist[self.n_c] += 1
            return self.n_c

    def reset_hist(self):
        """
        clears the histogram
        :return: None
        """
        self.hist = np.zeros(self.n_c)

    def prune_clusters(self,n=1):
        """
        This method removes any cluster with fewer than n samples assigned to it
        according to the histogram.
        :param n: the minimum histogram value for a cluster to remain
        :return:
        """
        self.hist = self.hist[:self.n_c]
        remove = np.arange(self.n_c)[self.hist<n]
        self.m = np.delete(self.m, remove, axis=0)
        self.d = np.delete(self.d, remove, axis=0)
        self.R = np.delete(self.R,remove)
        self.bins = np.delete(self.bins,remove)
        self.hist = np.delete(self.hist, remove)
        self.n_c = self.m.shape[0]
        return remove

    def run(self,X):
        return self.find_winning_cluster(X)

    def run_batch(self,X):
        """
        :param X: an nxm data matrix
        :return: the indices of the winning clusters for each sample
        Note: will return an index == self.n_c when sample does not resonate
        """
        if not data_validity_check(X):
            sys.exit('Invalid data. Normalize')
        C = np.zeros(X.shape[0])
        for i in range(X.shape[0]):
            C[i] = self.run(X[i,:])
        return C

    def train_online(self,X,res2func=None):
        """
        :param X: an m-dim feature vector
        :param res2func: an optional secondary resonance function f(c). for supervised learning.
        :return: the index of the winning cluster
        """
        c = self.find_winning_cluster(X,res2func)
        self.weight_update(X,c)
        return c

    def train_batch(self,X,epoch_max=None,change_min=0,shuffle=True,res2func=None):
        """
        :param X: an nxm data matrix
        :param epoch_max: the maximum number of training epochs
        :param change_min: the minimum change in weights at which to exit
        :param shuffle: boolean that indicates whether the samples should be shuffled
        :param res2func: an optional secondary resonance function f(i,c). for supervised learning.
        :return: the indices of the winning clusters for each sample
        """
        if not data_validity_check(X):
            sys.exit('Invalid data. Normalize')
        C = np.zeros(X.shape[0])
        Cidx = np.arange(X.shape[0])
        e = 0
        m = X.shape[0]
        untrained = True
        while untrained:
            e += 1
            m_old = np.copy(self.m)
            if shuffle:
                P = np.random.permutation(m)
                X = X[P,:]
                Cidx = Cidx[P]
            self.reset_hist()
            for i in range(m):
                if res2func is None:
                    C[i] = self.train_online(X[i,:])
                else:
                    C[i] = self.train_online(X[i,:],res2func=lambda c: res2func(i,c))
            print('Pruned:',len(self.prune_clusters(2)))
            if not np.any(np.equal(m_old,None)):
                change = np.abs(m_old.shape[0] - self.m.shape[0])
            else:
                change = self.m.shape[0]
            if change == 0:
                change = np.sum(np.abs(np.subtract(m_old,self.m)))/self.m.size
            untrained = (change > change_min) and (epoch_max is None or e < epoch_max)
            print('Epoch:',e,'  Clusters:',self.n_c,'  Change:',change)
        print('Trained')
        return C[np.argsort(Cidx)]

    def ellipse_c(self,c):
        x = np.copy(self.m[c,0])
        y = np.copy(self.m[c,1])
        a = np.copy(self.R[c])
        b = a*self.mu
        theta = np.arctan2(self.d[c,0],self.d[c,1])
        return ellipse(a,b,x,y,theta)



"""
A simple script to cluster random Data. The results are plotted in 2D
"""

if __name__ == '__main__':
    import matplotlib.pyplot as plt
    from VAT import VAT

    features = np.random.normal(0.4, 0.08, (100, 2))
    features[:, 0] = 0.2 * (features[:, 0] - 0.4) + 0.4
    features = np.vstack((features, np.random.normal(0.6, 0.05, (100, 2))))
    features[100:, 1] = 1.3 * (features[100:, 1])
    features = np.vstack((features, np.random.normal(0.7, 0.05, (100, 2))))
    features[200:, 1] = 0.5 * (1 - features[200:, 1])

    features = VAT(features)
    art = EllipsoidalART(0.01,0,0.4,0.8,1)
    art.train_batch(features,epoch_max=100)
    C = art.run_batch(features).flatten()
    plt.figure(1)
    s = np.int16(np.floor(255/art.n_c))
    for i in range(art.n_c+1):
        ps = features[C==i,:]
        for j in range(len(ps)):
            plt.plot(ps[j,0],ps[j,1],color=plt.cm.RdYlBu(s*i),marker='o')
    for c in range(art.n_c):
        E = art.ellipse_c(c)
        if E.a > 0:
            [ex,ey] = E.points()
            plt.plot(ex,ey,color=plt.cm.RdYlBu(s*c))
    plt.ylim(0,1)
    plt.xlim(0,1)
    plt.show()