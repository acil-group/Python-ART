"""
This is a general ART class that supports three methods as of this writing. Fuzzy, 2A, and ellipsoidal.
Methods are provided for both online and batch training as well as running. Always check data for validity
before running.

Written by Niklas Melton for the Applied Computational Intelligence Laboratory at Missouri S&T
"""
import numpy as np
import sys

valid_methods = ['Fuzzy','2A']

def data_validity_check(X):
    """
    :param X: an m-dim feature vector or an nxm data matrix
    :return: a boolean indicating the data validity
    """
    if np.max(X) > 1 or np.min(X) < 0:
        return 0
    else:
        return 1

def norm(X,l=1):
    """
    :param X: an m-dim feature vector
    :param l: the l-value of the norm (1,2,3,..,n)
    :return: the ln-norm of the feature vector
    """
    if l == 1:
        return np.sum(np.abs(X))
    elif l == 2:
        return np.sqrt(np.sum(np.power(X,2)))
    else:
        return np.power(np.sum(np.power(X, l)),1/l)

def fuzzy_and(X,Y):
    """
    :param X: an m-dim feature vector
    :param Y: an m-dim feature vector
    :return: the element-wise minimum of the two vectors
    """
    return np.minimum(X,Y)

def CC(X):
    """
    :param X: an m-dim feature vector or an nxm data matrix
    :return: the complement-coded feature vector or data matrix
    """
    if X.ndim == 1:
        X = np.reshape(X,(1,-1))
    return np.concatenate((X,1-X),axis=1)


class ART:
    def __init__(self,rho,alpha=0.0,beta=1.0,method='Fuzzy'):
        """
        :param rho: the vigilance parameter [0,1]
        :param alpha: the variance parameter [0,]
        :param beta: the learning parameter [0,1]
        :param method: the ART method used. One of valid_methods
        """
        self.rho = rho
        self.alpha = alpha
        self.beta = beta
        self.W = None
        self.n_c = 0
        self.hist = np.zeros(0)
        self.bins = []
        if method in valid_methods:
            self.method = method
        else:
            sys.exit('Invalid Method')

    def F1c(self,X,c):
        """
        :param X: an m-dim feature vector
        :param c: the cluster index of interest
        :return: the F1 activation of X on cluster c
        """
        if self.n_c == 0:
            sys.exit('No clusters. Initialize weights.')
        if self.n_c <= c:
            sys.exit('Invalid cluster selection')
        if self.method == 'Fuzzy':
            return norm(fuzzy_and(self.W[c,:],X))/norm(self.W[c,:])
        elif self.method == '2A':
            return np.dot(X, self.W[c, :]) / (norm(X, 2) * norm(self.W[i, :], 2))

    def F1(self,X):
        """
        :param X: an m-dim feature vector
        :return: the F1 activation of X on each of the clusters
        """
        F1 = np.zeros((self.n_c))
        for c in range(self.n_c):
            F1[c] = self.F1c(X, c)
        return F1

    def F2c(self,X,c):
        """
        :param X: an m-dim feature vector
        :param c: the cluster index of interest
        :return: the F2 activation of X on cluster c
        """
        if self.n_c == 0:
            sys.exit('No clusters. Initialize weights.')
        if self.n_c <= c:
            sys.exit('Invalid cluster selection')
        if self.method == 'Fuzzy':
            return norm(fuzzy_and(self.W[c,:],X))/norm(X) > self.rho
        elif self.method == '2A':
            return self.F1c(X,c) > self.rho

    def F2(self,X):
        """
        :param X: an m-dim feature vector
        :return: the F2 activation of X on each of the clusters
        """
        F2 = np.zeros((self.n_c))
        for c in range(self.n_c):
            F2[c] = self.F2c(X,c)
        return F2

    def F3c(self,X,c):
        """
        :param X: an m-dim feature vector
        :param c: the cluster index of interest
        :return: the F3 activation of X on cluster c
        """
        if self.method == '2A':
            F1c = self.F1c(X,c)
            return F1c*(F1c<self.rho)
        else:
            return np.multiply(self.F1c(X,c), self.F2c(X,c))

    def F3(self,X):
        """
        :param X: an m-dim feature vector
        :return: the F3 activation of X on each of the clusters
        """
        if self.n_c == 0:
            sys.exit('No clusters. Initialize weights.')
        if self.method == '2A':
            F3 = self.F1(X)
            F3[F3<self.rho] = 0
            return F3
        else:
            return np.multiply(self.F1(X),self.F2(X))

    def find_winning_cluster(self,X,res2func=None):
        """
        :param X: an m-dim feature vector
        :param res2func: an optional secondary resonance function f(c). for supervised learning.
        :return: the index of the winning cluster
        """
        if self.n_c == 0:
            self.hist = np.ones(1)
            return 0
        F3 = self.F3(X)
        mi = np.argmax(F3)
        while (not F3[mi]) or (res2func is not None and not res2func(mi)):
            F3[mi] = 0
            if np.count_nonzero(F3) == 0:
                break
            mi = np.argmax(F3)
        if F3[mi] > 0:
            self.hist[mi] += 1
            return mi
        else:
            if self.hist.shape[0] <= self.n_c:
                self.hist = np.concatenate((self.hist,np.ones(1)))
            else:
                self.hist[self.n_c] += 1
            return self.n_c

    def reset_hist(self):
        """
        clears the histogram
        :return: None
        """
        self.hist = np.zeros(self.n_c)

    def self_resonant(self,c1,c2):
        """
        checks if two clusters are self resonant
        :param c1: cluster index 1
        :param c2: cluster index 2
        :return: boolean of self resonance
        """
        return self.F2c(self.W[c1,:],c2) and self.F2c(self.W[c2,:],c1)

    def merge_clusters(self):
        """
        Merges clusters that are self resonant
        :return: None
        """
        w = None
        h = None
        Cs = np.arange(self.n_c)
        while len(Cs) > 0:
            new_w = np.copy(self.W[Cs[0],:])
            new_h = self.hist[0]
            for i in range(len(Cs)-1,0,-1):
                merge = self.self_resonant(Cs[0],Cs[i])
                if merge:
                    new_w = fuzzy_and(new_w,self.W[Cs[i],:])
                    new_h += self.hist[i]
                    Cs = np.delete(Cs,i)
            Cs = np.delete(Cs,0)
            if w is None:
                w = new_w
                h = new_h*np.ones(1)
            else:
                w = np.vstack((w,new_w))
                h = np.concatenate((h,np.ones(1)*new_h))
        self.W = w
        self.hist = h
        self.n_c = self.W.shape[0]


    def prune_clusters(self,n=1):
        """
        This method removes any cluster with fewer than n samples assigned to it
        according to the histogram.
        :param n: the minimum histogram value for a cluster to remain
        :return:
        """
        self.hist = self.hist[:self.n_c]

        remove = np.arange(self.n_c)[self.hist<n]
        self.W = np.delete(self.W,remove,axis=0)
        self.bins = np.delete(self.bins,remove)
        self.hist = np.delete(self.hist, remove)
        self.n_c = self.W.shape[0]
        return remove

    def weight_update(self,X,c):
        """
        Updates the weights of cluster c to reflect the addition of X
        :param X: an m-dim feature vector
        :param c: the index of the cluster to update
        :return: None
        """
        if self.method is 'Fuzzy':
            self.weight_update_fuzzy(X,c)
        elif self.method is '2A':
            self.weight_update_2A(X,c)

    def weight_update_fuzzy(self,X,c):
        """
        Updates the weights of cluster c to reflect the addition of X. Fuzzy clustering only
        :param X: an m-dim feature vector
        :param c: the index of the cluster to update
        :return: None
        """
        if self.W is None:
            self.W = np.reshape(np.copy(X),(1,-1))
            self.n_c = 1
        elif c >= self.n_c:
            self.W = np.vstack((self.W,np.copy(X)))
            self.n_c = self.W.shape[0]
        else:
            self.W[c,:] = self.beta*fuzzy_and(X,self.W[c,:]) + (1-self.beta)*self.W[c,:]

    def weight_update_2A(self,X,c):
        """
        Updates the weights of cluster c to reflect the addition of X. 2A clustering only
        :param X: an m-dim feature vector
        :param c: the index of the cluster to update
        :return: None
        """
        if self.W is None:
            self.W = np.reshape(np.copy(X),(1,-1))
            self.n_c = 1
        elif c >= self.n_c:
            self.W = np.vstack((self.W,np.copy(X)))
            self.n_c = self.W.shape[0]
        else:
            self.W[c, :] = self.beta*X + (1-self.beta)*self.W[c, :]



    def run(self,X,cc=True):
        """
        :param X: an m-dim feature vector
        :param cc: boolean indicating whether to compliment-code
        :return: the index of the winning cluster
        Note: will return an index == self.n_c when sample does not resonate
        """
        if cc and self.method=='Fuzzy':
            X = CC(X)
        return self.find_winning_cluster(X)

    def run_batch(self,X,cc=True):
        """
        :param X: an nxm data matrix
        :param cc: boolean indicating whether to compliment-code
        :return: the indices of the winning clusters for each sample
        Note: will return an index == self.n_c when sample does not resonate
        """
        if not data_validity_check(X):
            sys.exit('Invalid data. Normalize')
        C = np.zeros(X.shape[0])
        if cc and self.method=='Fuzzy':
            X = CC(X)
        for i in range(X.shape[0]):
            C[i] = self.find_winning_cluster(X[i,:])
        return C

    def train_online(self,X,cc=True,res2func=None):
        """
        :param X: an m-dim feature vector
        :param cc: boolean indicating whether to compliment-code
        :param res2func: an optional secondary resonance function f(c). for supervised learning.
        :return: the index of the winning cluster
        """
        if cc and self.method=='Fuzzy':
            X = CC(X)
        c = self.find_winning_cluster(X,res2func)
        self.weight_update(X,c)
        return c

    def train_batch(self,X,epoch_max=None,change_min=0,shuffle=True,cc=True,res2func=None):
        """
        :param X: an nxm data matrix
        :param epoch_max: the maximum number of training epochs
        :param change_min: the minimum change in weights at which to exit
        :param shuffle: boolean that indicates whether the samples should be shuffled
        :param cc: boolean indicating whether to compliment-code
        :param res2func: an optional secondary resonance function f(i,c). for supervised learning.
        :return: the indices of the winning clusters for each sample
        """
        if not data_validity_check(X):
            sys.exit('Invalid data. Normalize')
        if cc and self.method=='Fuzzy':
            X = CC(X)
        C = np.zeros(X.shape[0])
        Cidx = np.arange(X.shape[0])
        e = 0
        m = X.shape[0]
        change = 0
        untrained = True
        while untrained:
            e += 1
            W_old = np.copy(self.W)
            if shuffle:
                P = np.random.permutation(m)
                X = X[P,:]
                Cidx = Cidx[P]
            self.reset_hist()
            for i in range(m):
                if res2func is None:
                    C[i] = self.train_online(X[i,:], cc=False)
                else:
                    C[i] = self.train_online(X[i,:],cc=False,res2func=lambda c: res2func(i,c))
            self.merge_clusters()
            print('Pruned:',len(self.prune_clusters(2)))
            if not np.any(np.equal(W_old,None)):
                change = np.abs(W_old.shape[0] - self.W.shape[0])
            else:
                change = self.W.shape[0]
            if change == 0:
                change = np.sum(np.abs(np.subtract(W_old,self.W)))/self.W.size
            untrained = (change > change_min) and (epoch_max is None or e < epoch_max)
            print('Epoch:',e,'  Clusters:',self.n_c,'  Change:',change)
        print('Trained')
        return C[np.argsort(Cidx)]



"""
A simple script to cluster random data. The results are plotted in 2D
"""



if __name__ == '__main__':
    import matplotlib.pyplot as plt
    from VAT import VAT

    features = np.random.normal(0.4, 0.08, (100, 2))
    features[:, 0] = 0.2 * (features[:, 0] - 0.4) + 0.4
    features = np.vstack((features, np.random.normal(0.6, 0.05, (100, 2))))
    features[100:, 1] = 1.3 * (features[100:, 1])
    features = np.vstack((features, np.random.normal(0.7, 0.05, (100, 2))))
    features[200:, 1] = 0.5 * (1 - features[200:, 1])

    features = VAT(features)
    art = ART(0.7)
    art.train_batch(features)
    C = art.run_batch(features).flatten()
    plt.figure(1)
    for i in range(art.n_c+1):
        ps = features[C==i,:]
        for j in range(len(ps)):
            plt.plot(ps[j,0],ps[j,1],color=plt.cm.RdYlBu(20*i),marker='o')
    for i in range(art.n_c):
        box_x = [art.W[i,0],art.W[i,0],1-art.W[i,2],1-art.W[i,2],art.W[i,0]]
        box_y = [art.W[i,1],1-art.W[i,3],1-art.W[i,3],art.W[i,1],art.W[i,1]]
        plt.plot(box_x,box_y,color=plt.cm.RdYlBu(20*i))
    plt.ylim(0,1)
    plt.xlim(0,1)
    plt.show()











