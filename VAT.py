import numpy as np

def VAT(X, R=None, get_P=False, sample_pct=None):
    if sample_pct is None or sample_pct==1:
        A = np.copy(X)
    else:
        k = int(sample_pct*X.shape[0])
        S = np.random.choice(X.shape[0],k)
        A = np.copy(X[S,:])
    # A = np.reshape(A, [-1, 1])
    np.random.shuffle(A)
    n = A.shape[0]
    if R is None:
        R = np.zeros([n, n])
        for i in range(0, n):
            for j in range(0, i):
                R[i, j] = np.sum(np.power((A[i, :] - A[j, :]), 2))
                R[j, i] = R[i, j]
    P = np.zeros(n, dtype='int32')
    [i, j] = np.unravel_index(np.argmax(R), [n, n])
    P[0] = i
    I = [i]
    J = np.delete(np.arange(n, dtype='int32'), I)
    for r in range(1, n):
        R2 = R[:, J]
        R2 = R2[I, :]
        [i, j] = np.unravel_index(np.argmin(R2), R2.shape)
        j1 = J[j]
        P[r] = j1
        I.append(j1)
        J = np.delete(J, j)
    P = P.tolist()
    P.reverse()
    R = R[P, :]
    R = R[:, P]

    if get_P:
        if sample_pct is not None and sample_pct != 1:
            P = np.concatenate((S[P], np.delete(np.arange(X.shape[0]), S)))
            P = P.tolist()
        return P
    else:
        if sample_pct is not None and sample_pct != 1:
            A = np.concatenate((A[P,:],X[np.delete(np.arange(X.shape[0]), S)]))
        else:
            A = A[P,:]
        return A
